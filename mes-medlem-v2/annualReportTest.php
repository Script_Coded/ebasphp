<?php

require_once "ebas-api/ebasApi.class.php";

$ebasConn = new EbasApi\Connection("F140125-6", "MEsBoFGMJbd8Goxt4aXfpLtcaC5Ox1yc");

$testMember1 = new EbasApi\member($ebasConn);

$testMember1->fname = "Pelle1";
$testMember1->lname = "Test1";
$testMember1->adress = "Testgatan 1";
$testMember1->zipcode = "123 45";
$testMember1->city = "Teststaden";
$testMember1->ssn = "19490527-6962";
$testMember1->phone1 = "0987654321";
$testMember1->renewed = "2016-09-19";
$testMember1->memberFee = 50;

$testMember2 = new EbasApi\member($ebasConn);

$testMember2->fname = "Pelle2";
$testMember2->lname = "Test2";
$testMember2->adress = "Testgatan 1";
$testMember2->zipcode = "123 45";
$testMember2->city = "Teststaden";
$testMember2->ssn = "19490527-6962";
$testMember2->phone1 = "0987654321";
$testMember2->renewed = "2016-09-19";
$testMember2->memberFee = 50;

$testMember3 = new EbasApi\member($ebasConn);

$testMember3->fname = "Pelle3";
$testMember3->lname = "Test3";
$testMember3->adress = "Testgatan 1";
$testMember3->zipcode = "123 45";
$testMember3->city = "Teststaden";
$testMember3->ssn = "19490527-6962";
$testMember3->phone1 = "0987654321";
$testMember3->renewed = "2016-09-19";
$testMember3->memberFee = 50;

$report = new EbasApi\AnnualMeetingReport($ebasConn);
$report->addRepresentative($testMember1, EbasApi\TYPE_BOARD);
$report->addRepresentative($testMember2, EbasApi\TYPE_BOARD);
$report->addRepresentative($testMember3, EbasApi\TYPE_BOARD);

$report->date = "2016-09-20";
$report->auditByParent = true;
$report->guaranteeCorrect = true;
$report->memberFee = 50;

$report->push();

// $testMember->addAssociation("F130318-9");

// $testMember->push();