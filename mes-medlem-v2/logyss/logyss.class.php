<?php

/**
 * Login system library.
 * @author Malcolm Nihlén-Green <script_coded@hotmail.com>
 * @copyright 2016 Malcolm Nihlén-Green
 */
namespace Logyss;

require_once(dirname(__DIR__)."/lib/defuse-crypto.phar");
use Defuse\Crypto;

const DB_METHOD_SQLITE = "Sqlite";
const DB_METHOD_MYSQL = "MySql";

class AuthHandler {

	private $db = null;
	private $hashingOptions = ["cost" => 12];

	private $encryptionKey = null;

	public function __construct() {
		$args = func_get_args();
		switch ($args[0]) {
			case DB_METHOD_SQLITE:
				$this->db = new \PDO("sqlite:".$args[1]);
				break;
			case DB_METHOD_MYSQL:
				$this->db = new \PDO("mysql:host=".$args[1].";dbname=".$args[2], $args[3], $args[4]);
				break;
			default:
				throw new LogyssInvalidDBMethodAttributeException("Invalid DB method provided");
				break;
		}
	}

	public function login() {

	}

	/*
		Password strengths:
		* Characters >= 8
		* Characters >= 12
		* Both upper- and lowercase
		* Min 1 number
		* Special characters
	*/
	public function register(User $user, $reqPwLvl=0) {
		if (!isset($user->email)) {
			throw new LogyssRegisterCredentialException("Missing credential 'email'");
		}
		if (!isset($user->password)) {
			throw new LogyssRegisterCredentialException("Missing credential 'password'");
		}
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new LogyssRegisterEmailException("Invalid email adress");
		}
		if ($reqPwLvl > 0) {
			$pwLvl = 0;
			if (strlen($user->password) >= 8) {
				$pwLvl++;
			}
			if (strlen($user->password) >= 12) {
				$pwLvl++;
			}
		}

		// $sql = $this->db->prepare("INSERT INTO ");
	}

	public function getUserBy() {

	}

	public function loadEncryptionKeyFromFile($file) {
		$myfile = fopen($file, "r");
		$this->encryptionKey = Crypto\Key::loadFromAsciiSafeString(fread($myfile,filesize($file)));
		fclose($myfile);
		return true;
	}

	private function encryptString($data) {
		if (!isset($this->encryptionKey)) {
			throw new LogyssEncryptionKeyException("Encryption key not set");
		}
		return Crypto\Crypto::encrypt($data, $this->encryptionKey);
	}

	private function decryptString($ciphertext) {
		$decrypted = "";
		try {
			$decrypted = Crypto\Crypto::decrypt($ciphertext, $this->encryptionKey);
		} catch (Crypto\WrongKeyOrModifiedCiphertextException $ex) {
			throw new LogyssEncryptionModifiedException("Encryption modified");
		}
		return $decrypted;
	}
}

class User {
	public $email = null;
	public $password = null;

	public function __construct() {
		if ($this->getCustomParamaters() !== null) {
			foreach ($this->getCustomParamaters() as $key => $value) {
				$this->$key = null;
			}
		}
	}

	protected function getCustomParamaters() {
		return null;
	}
}

class LogyssInvalidDBMethodAttributeException extends \Exception{}
class LogyssEncryptionKeyMissingException extends \Exception{}
class LogyssEncryptionModifiedException extends \Exception{}
class LogyssRegisterCredentialException extends \Exception
class LogyssRegisterEmailException extends \Exception{}