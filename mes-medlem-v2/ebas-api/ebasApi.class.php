<?php

/**
 * Library for accessing Sverok eBas API.
 * @author Malcolm Nihlén-Green <script_coded@hotmail.com>
 * @copyright 2016 Malcolm Nihlén-Green
 */
namespace EbasApi;

const TYPE_BOARD = "Styrelse";
const TYPE_AUDITOR = "Revisor";
const TYPE_NOMINATING = "Valberedare";

class Connection {

	/**
	 * @var string $fid stores F-ID for the association
	 * @var string $apiKey stores API-key for the association
	 * @var array $apiUrls stores api URLs
	 * @var $useSSL stores where or not to use SSL. Should be set to true in production
	 */
	private $fid = "";
	private $apiKey = "";

	private $useSSL = false;

	private $apiUrls = array(
		"list_hobbies" => "https://ebas.sverok.se/apis/list_hobbies.json",
		"list_activity_questions" => "https://ebas.sverok.se/apis/list_activity_questions.json",
		"submit_member" => "https://ebas.sverok.se/apis/submit_member.json",
		"submit_annual_meeting_report" => "https://ebas.sverok.se/apis/submit_annual_meeting_report.json",
		"submit_annual_meeting_protocol" => "https://ebas.sverok.se/apis/submit_annual_meeting_protocol.json",
		"submit_activity_report" => "https://ebas.sverok.se/apis/submit_activity_report.json",
		"submit_child_association" => "https://ebas.sverok.se/apis/submit_child_association.json",
		"list_child_associations" => "https://ebas.sverok.se/apis/list_child_associations.json",
		"validate_members" => "https://ebas.sverok.se/apis/validate_members.json"
	);

	/**
	 * Sets $fid and $apiKey at instantiation
	 * 
	 * @var string $fid stores F-ID for the association
	 * @var string $apiKey stores API-key for the association
	 */
	public function __construct($fid, $apiKey) {
		$this->fid = $fid;
		$this->apiKey = $apiKey;
	}

	/**
	 * Saves a member to eBbas
	 */
	public function saveMember($member) {
		$associations = $member->associations;
		$associations[$this->fid] = $this->apiKey;

		if (!isset($member->fname) || $member->fname == "") {
			throw new EbasMemberAttributeException("Member attribute 'fname' not set");
		}
		if (!isset($member->lname) || $member->lname == "") {
			throw new EbasMemberAttributeException("Member attribute 'lname' not set");
		}
		if (!isset($member->adress) || $member->adress == "") {
			throw new EbasMemberAttributeException("Member attribute 'adress' not set");
		}
		if (!isset($member->zipcode) || $member->zipcode == "") {
			throw new EbasMemberAttributeException("Member attribute 'zipcode' not set");
		}
		if (!isset($member->city) || $member->city == "") {
			throw new EbasMemberAttributeException("Member attribute 'city' not set");
		}
		if (!isset($member->ssn) || $member->ssn == "") {
			throw new EbasMemberAttributeException("Member attribute 'ssn' not set");
		}
		if (!isset($member->phone1) || $member->phone1 == "") {
			throw new EbasMemberAttributeException("Member attribute 'phone1' not set");
		}
		if (!isset($member->renewed) || $member->renewed == "") {
			throw new EbasMemberAttributeException("Member attribute 'renewed' not set");
		}

		$request = array(
			"request" => array(
				"action" => "list_child_associations",
				"version" => "2015-06-01",
				"association_number" => $this->fid,
				"api_key" => $this->apiKey
			)
		);

		$data_string = json_encode($request);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrls["list_child_associations"]);
		 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->useSSL);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
		);
		 
		$result=json_decode(curl_exec($ch), true);
		curl_close($ch);

		foreach ($result["response"] as $association) {
			if (isset($associations[$association["Association"]["association_number"]])) {
				$associations[$association["Association"]["association_number"]] = $association["Association"]["api_key"];
				$fid = $association["Association"]["association_number"];

				$request = array(
					"api_key" => $associations[$fid],
					"member" => array(
						"firstname" => $member->fname,
						"lastname" => $member->lname,
						"renewed" => $member->renewed,
						"street" => $member->adress,
						"co" => (isset($member->co) && $member->co != "") ? $member->co : [],
						"zip_code" => $member->zipcode,
						"city" => $member->city,
						"country" => (isset($member->country) && $member->country != "") ? $member->country : [],
						"socialsecuritynumber" => $member->ssn,
						"email" => $member->email,
						"phone1" => $member->phone1,
						"phone2" => (isset($member->phone2) && $member->phone2 != "") ? $member->phone2 : [],
						"member_fee" => (isset($member->memberFee) && $member->memberFee != "") ? $member->memberFee : []
					)
				);
				$data_string = json_encode($request);
				 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $this->apiUrls["submit_member"]);
				 
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->useSSL);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($data_string))
				);
				 
				$result=json_decode(curl_exec($ch));
				curl_close($ch);

				if (isset($result->member_errors)) {
					foreach ($result->member_errors as $error) {
						throw new EbasResponseException("'".$error[0]."'");
					}
				}
				if ($result->request_result == "success") {
					var_dump(json_encode($result));
				}
			}
		}
	}

	/**
	 * Saves an annual meeting report to eBbas
	 */
	public function saveAnnualMeetingReport($report) {
		if (!isset($report->date) || $report->date == "") {
			throw new EbasReportAttributeException("Report attribute 'date' not set");
		}
		if (!isset($report->auditByParent)) {
			throw new EbasReportAttributeException("Report attribute 'auditByParent' not set");
		}
		if (!isset($report->guaranteeCorrect)) {
			throw new EbasReportAttributeException("Report attribute 'guaranteeCorrect' not set");
		}
		if (!isset($report->memberFee)) {
			throw new EbasReportAttributeException("Report attribute 'memberFee' not set");
		}
		$boardCount = 0;
		foreach ($report->representatives as $representative) {
			if ($representative["type"] == TYPE_BOARD) {
				$boardCount += 1;
			}
		}
		if ($boardCount < 3) {
			throw new EbasBoardSizeException("Minimum board size of 3 required");
		}
		if (!$report->auditByParent) {
			$validAudit = false;
			foreach ($report->representatives as $representative) {
				if ($representative["type"] == TYPE_AUDITOR) {
					$validAudit = true;
				}
			}
			if (!$validAudit) {
				throw new EbasAuditException("Either 'auditByParent' must be true, or an auditor must be in the report");
			}
		}

		$request = array(
			"request" => array(
				"action" => "submit_annual_meeting_report",
				"version" => "2015-06-01",
				"association_number" => $this->fid,
				"api_key" => $this->apiKey,
				"data" => array(
					"AnnualMeetingReport" => array(
						"date" => $report->date,
						"updated_bylaws" => (isset($report->updatedBylaws) && $report->updatedBylaws != "") ? $report->updatedBylaws : [],
						"member_fee" => $report->memberFee,
						"audit_by_parent" => $report->auditByParent,
						"guarantee_correct" => $report->guaranteeCorrect
					),
					"Representative" => $report->representatives
				)
			)
		);
		$data_string = json_encode($request);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrls["submit_annual_meeting_report"]);
		 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->useSSL);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
		);
		 
		$result=json_decode(curl_exec($ch));
		curl_close($ch);
		echo json_encode($result);
		// if (isset($result->response->validation_error)) {
		// 	foreach ($result->response->validation_error as $error) {
		// 		throw new EbasResponseException("'".$error[0]."'");
		// 	}
		// }

		// if ($result->response->request_result == "success") {
		// 	var_dump(json_encode($result));
		// }
	}

	public function listChildAssociations() {
		$request = array(
			"request" => array(
				"action" => "list_child_associations",
				"version" => "2015-06-01",
				"association_number" => $this->fid,
				"api_key" => $this->apiKey
			)
		);

		$data_string = json_encode($request);
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrls["list_child_associations"]);
		 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->useSSL);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
		);
		 
		$result=json_decode(curl_exec($ch), true);
		curl_close($ch);

		return $result;
	}
}

class Member {

	/**
	 * @var Connection $ebasConn stores eBas Connection object
	 */

	private $ebasConn = null;

	public $associations = array();

	public $fname = "";
	public $lname = "";
	public $renewed = "";
	public $adress = "";
	public $co = "";
	public $zipcode = "";
	public $city = "";
	public $coutry = "";
	public $ssn = "";
	public $email = "";
	public $phone1 = "";
	public $phone2 = "";
	public $memberFee = 0;

	public function __construct(&$ebasConn) {
		$this->ebasConn = $ebasConn;
	}

	/**
	 * Add an association to the push list
	 */
	public function addAssociation($fid) {
		$this->associations[$fid] = [];
	}

	/**
	 * Remove an association from the push list.
	 * This won't remove the user from the association if the user is already a member.
	 */
	public function removeAssociation($fid) {
		if (isset($this->associations[$fid])) {
			unset($this->associations[$fid]);
		}
	}

	/**
	 * Return the push list of associations to be pushed to eBas
	 */
	public function listAssociations() {
		return array_keys($this->associations);
	}

	/**
	 * Pushes user to eBas
	 */
	public function push() {
		$this->ebasConn->saveMember($this);
	}
}

class AnnualMeetingReport {

	private $ebasConn = null;

	public $representatives = array();

	public $date = "";
	public $updatedBylaws = "";
	public $memberFee = null;
	public $auditByParent = false;
	public $guaranteeCorrect = null;

	function __construct(&$ebasConn) {
		$this->ebasConn = $ebasConn;
	}

	function addRepresentative($member, $type, $representativeTitle="") {
		$representative = array();
		$representative["fistname"] = $member->fname;
		$representative["lastname"] = $member->lname;
		$representative["socialsecuritynumber"] = $member->ssn;
		$representative["phone"] = $member->phone1;
		$representative["email"] = (isset($member->email) && $member->email != "") ? $member->email : "";
		$representative["type"] = $type;
		$representative["representative_title"] = $representativeTitle;
		array_push($this->representatives, $representative);
	}

	function listRepresentatives() {
		return $this->representatives;
	}

	function push() {
		$this->ebasConn->saveAnnualMeetingReport($this);
	}
}

class EbasMemberAttributeException extends \Exception{}
class EbasReportAttributeException extends \Exception{}
class EbasBoardSizeException extends \Exception{}
class EbasResponseException extends \Exception{}
class EbasAuditException extends \Exception{}