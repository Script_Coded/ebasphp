<?php

require_once "logyss/logyss.class.php";

class MESUser extends Logyss\User {
	public $customProperties = array(
		"fname" => "string:encrypted",
		"lname" => "string:encrypted"
	);

	function getCustomParamaters() {
		return $this->customProperties;
	}
}

$authHandler = new Logyss\AuthHandler(Logyss\DB_METHOD_SQLITE, dirname(__FILE__)."/database.sqlite");

// $props = new Logyss\UserPropertyGroup(array("test"));

$user = new MESUser();
$user->email = "test@example.com";
$user->password = "123";

$authHandler->register($user);

$authHandler->loadEncryptionKeyFromFile("private/encryptionkey.txt");



// $key = Crypto\Key::createNewRandomKey();
// echo $key->saveToAsciiSafeString();

// var_dump($props);