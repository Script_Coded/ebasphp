<?php

// plain text password
$password = 'secretcode';

// The cost parameter can change over time as hardware improves
$options = ['cost' => 12];

$hashed = password_hash($password, PASSWORD_DEFAULT, $options);

echo password_verify("secretcode", $hashed);